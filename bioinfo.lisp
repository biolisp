(in-package :bioinfo)

(clsql:enable-sql-reader-syntax)

(defparameter *system-path*
  (asdf:component-pathname (asdf:find-component nil :bioinfo)))
