(in-package :bioinfo)

(defparameter *sagemaps* '(:genie   (:human (:short #p"data/genie/Hs_short.best_gene"
					     :long  #p"data/genie/Hs_long.best_gene"))
			   :sagemap (:human (:short #p"data/sagemap_new/SAGEmap_Hs_NlaIII_10_best"))
			   :sagemap_old (:human (:short #p"data/sagemap/HS/NLAIII/SAGEmap_tag_ug-rel"
					    :long  #p"data/sagemap/HS/NLAIII/SAGEmap_tag_ug-rel")))
  "plist of available sagemaps")


(defun find-sagemap (library organism tag-kind)
  (getf (getf (getf *sagemaps* library ) organism) tag-kind))

(defun update-sagemap (tag map lst)
  (setf (third lst) (read-from-string (third lst)))
  (let ((cur-tag (gethash tag map)))
    (if cur-tag
	(when (> (third lst) (second cur-tag))
	  (setf (gethash tag map) (rest lst)))
	(setf (gethash tag map) (rest lst)))))


(defun update-sage-genie (tag map lst)
  (setf (gethash tag map) (rest lst)))

(defun load-tagmap-file (filespec tag-kind update-map-fun)
  "Load the sagemap into a hash table"
  (let ((map (make-hash-table :test 'equal :size 10000)))
    (with-open-file (in filespec :external-format :ascii)
      (each-line (line in)
	(let* ((lst (split-sequence #\Tab line))
	       (tag (first lst)))
	  (ecase tag-kind
	    (:short 
	     (when (eq 10 (length tag))
	       (funcall update-map-fun tag map lst)))
	    (:long 
	     (when (> (length tag) 10)
	       (funcall update-map-fun tag map lst)))))))
    map))


(defun load-sagemap (&key (organism :human) (tag-kind :short))
  "Load the apropriate library"
  (load-tagmap-file 
   (merge-pathnames (find-sagemap :sagemap organism tag-kind)   *system-path*) 
   tag-kind
   #'update-sagemap))

(defun load-sage-genie (&key (organism :human) (tag-kind :short))
  "Load the apropriate library"
  (load-tagmap-file 
   (merge-pathnames (find-sagemap :genie organism tag-kind)   *system-path*) 
   tag-kind
   #'update-sage-genie))

(defun map-tag (tag map)
  "Map a single tag to the given map"
  (multiple-value-bind (value founded) 
      (gethash (string-upcase tag) map)
    (if founded
	value
	(list "[* TAG NOT FOUNDED IN MAP *]"))))


(defun map-each-file-line (file map)
  "map-tag first column of the given file"
  (with-open-file (in file)
    (each-line (line in)
      (let ((lst (split-sequence #\Tab line)))
	(fresh-line)
	(if (equal (string-downcase (first lst)) "tag")
	    (progn
	      (princ line)
	      (princ #\Tab)
	      (princ "Annotation"))
	    (progn
	      (princ line)
	      (princ #\Tab)
	      (format t "~{~a~^	~}" (map-tag (first lst) map))))))))



(defun annotate-tags-file (tagsfile map &key suffix)
  "Read a file with tags in the first column, and
save the annotation to another file with same name, but with
extension ann.csv"
  (let* ((tagsfile (make-pathname :defaults tagsfile))
	 (output-name (concatenate 'string (pathname-name tagsfile) "-" suffix)))
    (redirect  (make-pathname :name output-name :type "csv" :defaults tagsfile)
      (map-each-file-line tagsfile map)))
  ;; Imediatly call gc after mapping
  (gc))


(defun sagemap-all-file (dir tags &key (suffix "annotation"))
  (cl-fad:walk-directory dir
			 #'(lambda (file)
			     (annotate-tags-file file tags :suffix suffix))
			 :test 
			 #'(lambda (file)
			     (equal (pathname-type file) "txt"))))
