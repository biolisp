;; We use cl-ppcre for text munging
(require 'cl-ppcre)
(require 'cl-pdf)

(defparameter *HELV* (pdf:get-font "Helvetica"))
(defparameter *HELV-SIZE* 12.0)
(defparameter *CLUST-HEIGHT* 20)

;; Dummy function used in delete-if
(defun truep (x) (declare (ignore x)) t)

(defun readfile (filename)
  (with-open-file (stream filename)
    (let* ((whitespace '(#\Space #\Tab #\Newline #\Linefeed #\Return))
           (rownames (make-array 0 :fill-pointer 0 :adjustable t))
           (data (make-array 0 :fill-pointer 0 :adjustable t))
           (colnames (map 'vector #'(lambda (x) x) (cdr (cl-ppcre:split #\Tab (string-trim whitespace (read-line stream nil nil))))))
           p)
      (loop for line = (read-line stream nil nil)
         while line do (progn
                         (setf p (cl-ppcre:split #\Tab (string-trim whitespace line)))
                         (vector-push-extend (car p) rownames)
                         (vector-push-extend (map 'vector #'(lambda (x) (float (parse-integer x))) (cdr p)) data)))
      ;; subseq's lock down adjustable vectors into simple vectors
      (list (subseq rownames 0) colnames (subseq data 0)))))

(defun pearson-distance (v1 v2)
  (let* ((n (length v1))
         (sum1 (reduce #'+ v1))
         (sum2 (reduce #'+ v2))
         (sum1-sq (reduce #'+ (map 'vector #'(lambda (x) (* x x)) v1)))
         (sum2-sq (reduce #'+ (map 'vector #'(lambda (x) (* x x)) v2)))
         (psum (reduce #'+ (map 'vector #'* v1 v2)))
         (num (- psum (/ (* sum1 sum2) n)))
         (den (sqrt (* (- sum1-sq (/ (* sum1 sum1) n)) (- sum2-sq (/ (* sum2 sum2) n))))))
    (if (zerop den) 0 (- 1.0 (/ num den)))))

(defclass bicluster ()
  ((vec :initarg :vec :accessor vec :initform nil)
   (left :initarg :left :accessor left :initform nil)
   (right :initarg :right :accessor right :initform nil)
   (id :initarg :id :accessor id :initform nil)
   (distance :initarg :distance :accessor distance :initform 0.0)))


(defun hcluster (rows &optional (distance 'pearson-distance))
  (let* ((distances (make-hash-table :test 'equalp))
         (current-clust-id -1)
         (rowlen (length rows))
         (clust (make-array rowlen :fill-pointer rowlen 
                                   :adjustable t 
                                   :initial-contents (loop for i upto (1- rowlen) collect (make-instance 'bicluster :vec (aref rows i) :id i)))))
    (do* (lowestpair closest d mergevec newcluster)
         ((<= (length clust) 1) (aref clust 0))
      (setf lowestpair (cons 0 1))
      (setf closest (funcall distance (vec (aref clust 0)) (vec (aref clust 1)))) 
      (dotimes (i (length clust))
        (do ((j (1+ i) (1+ j)))
            ((>= j (length clust)))
          (let* ((iid (id (aref clust i)))
                 (jid (id (aref clust j)))
                 (key (cons iid jid)))
            (unless (gethash key distances)
              (setf (gethash key distances) (funcall distance (vec (aref clust i)) (vec (aref clust j)))))
            (setf d (gethash key distances))
            (when (< d closest)
              (setf closest d)
              (setf lowestpair (cons i j))))))
      (setf mergevec (map 'vector #'(lambda (x y) (/ (+ x y) 2.0)) 
                          (vec (aref clust (car lowestpair))) 
                          (vec (aref clust (cdr  lowestpair)))))
      (setf newcluster (make-instance 'bicluster :vec mergevec
                                      :left (aref clust (car lowestpair))
                                      :right (aref clust (cdr lowestpair))
                                      :distance closest
                                      :id current-clust-id))
      (decf current-clust-id)
      (setf clust (delete-if #'truep clust :start (cdr lowestpair) :end (1+ (cdr lowestpair))))
      (setf clust (delete-if #'truep clust :start (car lowestpair) :end (1+ (car lowestpair))))
      (vector-push-extend newcluster clust))))
             

(defun printclust (clust &optional labels (n 0))
  (let ((cid (id clust)))
    (format t "~va" n #\Space)
    (if (< cid 0)
        (format t "-~%")
        (if labels
            (format t "~a~%" (aref labels cid))
            (format t "~a~%" cid)))
    (when (left clust)
      (printclust (left clust) labels (1+ n)))
    (when (right clust)
      (printclust (right clust) labels (1+ n)))))

(defun getheight (clust)
  (if (and (left clust) (right clust))
      (+ (getheight (left clust)) (getheight (right clust))) 
      1))

(defun getdepth (clust)
  (if (and (left clust) (right clust))
      (max (getdepth (left clust)) (+ (getdepth (right clust)) (distance clust))) 
      0.0))

(defun draw-line (x1 y1 x2 y2)
  (pdf:move-to x1 y1)
  (pdf:line-to x2 y2)
  (pdf:stroke))

(defun drawdendogram (clust labels file)
  (let* ((h (* (getheight clust) *CLUST-HEIGHT*))
         (w 1200)
         (depth (getdepth clust))
         (scaling (float (/ (- w 150) depth)))
         ; Add to w for long blog names
         (bounds (make-array 4 :initial-contents (list 0 0 (+ w 200) h))))
    (pdf:with-document ()
      (pdf:with-page (:bounds bounds)
        (pdf:set-line-width 0.1)
        
        (draw-line 0 (/ h 2) 10 (/ h 2))
        
        (drawnode clust 10 (/ h 2) scaling labels))
      (with-open-file (s file :direction :output :if-exists :supersede
                         ; :element-type '(unsigned-byte 8)
                         :element-type :default
                         :external-format :latin-1)
        (pdf:write-document s)))))

(defun drawnode (clust x y scaling labels)
  (if (< (id clust) 0)
      (let* ((h1 (* (getheight (left clust)) *CLUST-HEIGHT*))
             (h2 (* (getheight (right clust)) *CLUST-HEIGHT*))
             (avg (/ (+ h1 h2) 2))
             (top (- y avg))
             (bottom (+ y avg))
             (ll (* (distance clust) scaling)))

        ; Vertical line form this cluster to children
        (draw-line x (+ top (/ h1 2)) x (- bottom (/ h2 2)))
        ; horizontal line to left item
        (draw-line x (+ top (/ h1 2)) (+ x ll) (+ top (/ h1 2)))
        ; horizontal line to right item
        (draw-line x (- bottom (/ h2 2)) (+ x ll) (- bottom (/ h2 2)))

        ; recurse
        (drawnode (left clust) (+ x ll) (+ top (/ h1 2)) scaling labels)
        (drawnode (right clust) (+ x ll) (- bottom (/ h2 2)) scaling labels))
      (progn
        (pdf:in-text-mode
          (pdf:set-font *HELV* *HELV-SIZE*)
          (pdf:move-text (+ x 5) (- y 7))
          (pdf:draw-text (aref labels (id clust)))))))
