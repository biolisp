(in-package :cluster)

(declaim (optimize (speed 3) (debug 0)))

(declaim (inline diff-of-squares))
(declaim (ftype (function (number number) float) square-of-difference))
(defun square-of-difference (a b)
  (sq (- a b)))


(declaim (inline diff-of-squares))
(declaim (ftype (function (number number) float) absolute-difference))
(defun absolute-difference (a b)
  (abs (- a b)))

(defmacro def-vec2fun ((name) documentation &body body)
  "Declare a function 'name' of two vectors, a and b. 
Add the optiomization declare and array dimension verification"
  `(declaim (ftype (function (simple-array simple-array) float) ,name))
  `(defun ,name (a b)
     ,documentation
     (assert (= (array-dimension a 0)
		(array-dimension b 0)))
     ,@body))



;;; Simple Distances

(def-vec2fun (euclidean-distance)    
  "Calculate the Euclidean distance between two vectors"
  (sqrt (reduce '+ (map 'vector #'square-of-difference a b))))

(def-vec2fun (manhatan-distance)
  "Manhatan, or city block, or norm-1 distance"
  (reduce '+ (map 'vector #'absolute-difference a b)))


;; Pearson distance

(defun vectors-statistics (a b)
  (loop for i from 0 to (1- (array-dimension a 0))
       for x = (aref a i)
       for y = (aref b i)
     summing x into Sx
     summing y into Sy
     summing (sq x) into Sx2
     summing (sq y) into Sy2
     summing (* x y) into Sxy
     finally (return (values Sx Sy Sx2 Sy2 Sxy))))
       

(def-vec2fun (pearson-correlation)
    "Pearson correlation"
  (let ((N (array-dimension a 0)))
    (mvbind (Sx Sy Sx2 Sy2 Sxy) (vectors-statistics a b)
	    (/ (- Sxy (/ (* Sx Sy) N))
	       (sqrt (* (- Sx2 (/ (sq Sx) N))
			(- Sy2 (/ (sq Sy) N))))))))

(def-vec2fun (pearson-distance)
    "Pearson distance"
    (- 1.0 (pearson-correlation a b)))

(def-vec2fun (absolute-pearson-distance)
    "Absolute Pearson distance"
    (- 1.0 (abs (pearson-correlation a b))))

(def-vec2fun (square-pearson-distance)
    "Square Pearson distance"
    (- 1.0 (sq (pearson-correlation a b))))
