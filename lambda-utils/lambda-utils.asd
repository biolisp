(defpackage #:lambda-utils-system
  (:use :cl :asdf))

(in-package #:lambda-utils-system)

(defsystem :lambda-utils
  :description "Misc utilities."
  :version "0.1"
  :author "Leonardo Varuzza <varuzza@gmail.com>"
  :licence "GPL"
  :serial t
  :depends-on (:cl-utilities :gzip-stream :flexi-streams)
  :components ((:file "packages")
	       (:file "abbrev")	       
	       (:file "paip")
	       (:file "onlisp")
	       (:file "list")
	       (:file "string")
	       (:file "ext")
	       (:file "hash")
	       (:file "IO-macros")
	       (:file "IO")
	       (:file "mime")
	       (:file "head")
	       (:file "numbers")
	       (:file "vector")))
