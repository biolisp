(in-package :lambda-utils)

(defun na-p (x)
  (eq x :na))


(declaim (inline sq)
	 (ftype (function (number) number) sq))

(defun sq (x)
  "Square of a number"
  (* x x))
