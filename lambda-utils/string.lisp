(in-package :lambda-utils)

(declaim (optimize (speed 3) (debug 1) (safety 1)))

(declaim (inline join))
(defun join (sep list)
  "join list elements into a string, with values separated by sep."
  (let ((f (format nil "~~{~~a~~^~a~~}" sep)))
    (format nil f list)))


;; Conses more then split-sequence
(defun split-string (c str)
  "Split STR, returning a list of strings corresponding to the
substrings of STR which are separated by occurrences of C."
  (declare (type character c)
	   (type string str))
  (flet ((split1 (c str)
		 "SPLIT1 splits STR at the first occurrence of the
		 character C, returning, as multiple values, the substring
		 upto but not including C as the first value, and the
		 substring from immediately after C and extending to the end
		 of STR."
		 (declare (type character c)
			  (type string str))
		 (let ((pivot (position c str)))
		   (if (null pivot)
		       (values str)
		     (values (subseq str 0 pivot)
			     (subseq str (1+ pivot)))))))
	(if (= 0 (length str))
	    ()
	  (multiple-value-bind (first rest) (split1 c str)
			       (if (null rest)
				   (cons first nil)
				 (if (= 0 (length rest))
				     (cons first '(""))
				   (cons first (split-string c rest))))))))

 (defun string-tokens (string &key (start 0) max)
  "Read from STRING repeatedly, starting with START, up to MAX tokens.
Return the list of objects read and the final index in STRING.
Binds `*package*' to the keyword package,
so that the bare symbols are read as keywords."
  (declare (type (or null fixnum) max) (type fixnum start))
  (let ((*package* (find-package :keyword)))
    (if max
        (do ((beg start) obj res (num 0 (1+ num)))
            ((= max num) (values (nreverse res) beg))
          (declare (fixnum beg num))
          (setf (values obj beg)
                (read-from-string string nil +eof+ :start beg))
          (if (eq obj +eof+)
              (return (values (nreverse res) beg))
              (push obj res)))
        (read-from-string (concatenate 'string "(" string ")")
                          t nil :start start))))



(defun puts (x &optional (s *standard-output*))
  "Write x as a string and a new line (like ruby puts)"
  (format s "~a~%" x))


(defun split-sequence-array (delimiter seq &key (count nil) (remove-empty-subseqs nil) (from-end nil) (start 0) (end nil) (test nil test-supplied) (test-not nil test-not-supplied) (key nil key-supplied))
  "Return an array of subsequences in seq delimited by delimiter.

If :remove-empty-subseqs is NIL, empty subsequences will be included
in the result; otherwise they will be discarded.  All other keywords
work analogously to those for CL:SUBSTITUTE.  In particular, the
behaviour of :from-end is possibly different from other versions of
this function; :from-end values of NIL and T are equivalent unless
:count is supplied. The second return value is an index suitable as an
argument to CL:SUBSEQ into the sequence indicating where processing
stopped."
  (let* ((len (length seq))
	 (n (if count
		count
		(1+ (count delimiter seq))))
	 (subseqs (make-array n :initial-element nil :adjustable nil :fill-pointer nil :displaced-to nil))
	 (other-keys (nconc (when test-supplied 
			      (list :test test))
			    (when test-not-supplied 
			      (list :test-not test-not))
			    (when key-supplied 
			      (list :key key)))))
    (declare (fixnum n))
    (unless end (setq end len))
    (if from-end
        (loop for right = end then left
	   for left = (max (or (apply #'position delimiter seq 
				      :end right
				      :from-end t
				      other-keys)
			       -1)
			   (1- start))
	   unless (and (= right (1+ left))
		       remove-empty-subseqs) ; empty subseq we don't want
	   if (and count (>= nr-elts count))
	   ;; We can't take any more. Return now.
	   return (values (nreverse subseqs) right)
	   else 
	   do (progn (setf (aref subseqs nr-elts) (subseq seq (1+ left) right)) (print nr-elts))
	   and sum 1 into nr-elts
	   until (< left start)
	   finally (return (values (nreverse subseqs) (1+ left))))
	(loop
	   for left = start then (+ right 1)
	   for right = (min (or (apply #'position delimiter seq 
				       :start left
				       other-keys)
				len)
			    end)
	   unless (and (= right left) 
		       remove-empty-subseqs) ; empty subseq we don't want
	   if (and count (>= nr-elts count))
	   ;; We can't take any more. Return now.
	   return (values subseqs left)
	   else
	   do (setf (aref subseqs nr-elts) (subseq seq left right))
	   and sum 1 into nr-elts
	   until (>= right end)
	   finally (return (values subseqs right))))))
  