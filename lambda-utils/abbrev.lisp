(in-package :lambda-utils)

(defmacro abbrev  (short long)
  `(defmacro ,short (&rest args)
    `(,',long ,@args)))

(abbrev mvbind multiple-value-bind)
