(in-package :cl-user)

(set-dispatch-macro-character #\# #\{                 ; Dispatch on the sequence #[ or #arg[
  #'(lambda (stream subchar arg)                      ; Anonymous function to read/parse it
      (declare (ignore subchar))                      ; We already know it is [
      (let ((list (read-delimited-list #\} stream t)) ; Read in the rest of the list, up to ]
	    (keys '())                                ; Empty list, filled below
	    (values '())                              ; Empty list, filled below
            (hashtab (gensym)))                       ; Gensym name for the hashtab so the values can't clobber it

	(do ((key list (cddr key))                    ; Loop for keys being sublists, skipping 2 ahead each time
	     (value (cdr list) (cddr value)))         ; ...and for values being sublists, skipping 2 ahead
	    ((null key))                              ; Terminate loop when out of keys
	  (push (car key) keys)                       ; Assemble the keys in reverse order
	  (push (car value) values))                  ; Assemble value forms in reverse order
	(setf keys (nreverse keys))                   ; Reverse the keys - push/nreverse is the fast way to do this
	(setf values (nreverse values))               ; Reverse the value forms

        ;;; The next 8 lines are the code template
	`(let ((,hashtab ,(if arg                 ; If there is an argument given, make the hash-table that size
			      `(make-hash-table :test #'equalp :size ,arg)
			      '(make-hash-table :test #'equalp))))        ; Otherwise use the default size
	   ,@(mapcar #'(lambda (key value)                                ; Map this function across keys/values
			 `(setf (gethash ',key ,hashtab) ,value))         ; Add the item to the hash
		     keys
		     values)
	   ,hashtab))))                                                   ; Return the generated hashtab

(set-macro-character #\{                                                  ; Dispatch on [
  #'(lambda (stream char)                                                 ; Anonymous function to read/parse
      (declare (ignore char))                                             ; We already know that it's [
      (let ((list (read-delimited-list #\} stream t)))                    ; Read up through ]
	(when (/= (length list) 2)                                        ; Make sure that we have two elements
	  (error "Invalid number of arguments to []"))
	(when (not (symbolp (cadr list)))                                 ; Make sure that the key is a symbol
	  (error "Key must be a symbol"))
	`(gethash ',(cadr list) ,(car list)))))                           ; The actual code template

(set-macro-character #\} (get-macro-character #\)))                       ; This is a helper for read-delimited-list
