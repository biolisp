(defpackage :lambda-utils
  (:nicknames :lutil)
  (:use :cl :cl-utilities :flexi-streams  :gzip-stream)
  (:export
   ;; abrev
   mvbind
   ;; String
   join
   split-sequence-array
   string-tokens
   puts
   ;; PAIP
   mappend
   ;; onlisp
   rfind-if
   rfind

   ;;list
   list-partition
   
   ;; IO
   each-line
   def-input-fun
   def-output-fun
   slurp
   colect-lines
   redirect
   
   ;; Mime
   mime-type
   typed-open
   with-open-typed-file
   
   ;; numbers
   na-p
   sq
   ;; vector
   dovector
   doindex
   ;; Print-head
   print-head
   ;; Ext
   defsubst
   defcustom
   defconst
   map-in
   gc
   quit
   +eof+
   eof-p
   remove-plist
   ;; Shell
   run-prog
   exec-prog
   exec-prog-l
   getenv
   pipe-output
   pipe-input
   close-pipe
   with-open-pipe
   ))

