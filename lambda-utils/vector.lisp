(in-package :lambda-utils)

(defmacro dovector ((var vector) &body body)
  (let ((index (gensym)))
    `(loop for ,index from 0 to (1- (array-dimension ,vector 0)) 
	for ,var = (aref ,vector ,index) do ,@body)))

(defmacro doindex ((index vector) &body body)
  `(loop for ,index from 0 to (1- (array-dimension ,vector 0)) 
      do ,@body))
