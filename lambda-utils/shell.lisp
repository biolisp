(in-package :lambda-utils)

(defun getenv (var)
  "Return the value of this environment variable"
  #+sbcl(sb-posix:getenv var)
  #-sbcl(error "getenv: Not implemented for this Common Lisp"))


(defun find-in-path (prog)
  "Find prog in system path"
  (let ((paths (split-sequence #\: (getenv "PATH"))))
    (dolist (dir paths)
      (let ((path (make-pathname :directory dir :name prog)))
	(when (probe-file path)
	  (return-from find-in-path path))))
    (error  "Program ~a not found in path ~a" prog paths)))       ; TODO: Use cerror here



(defun exec-prog-l (prog args)
  "Find prog in the path and execute it  with args given by a list.
Version with args in a list.
Example: (exec-prog-l \"echo\" '(\"a\" \"b\")) ==> a b "
  (run-prog (find-in-path prog) :args args))

(defun exec-prog (prog &rest args)
  "Find prog in the path and execute it  with args given by a list
Example: (exec-prog \"echo\" \"a\" \"b\") ==> a b "
  (exec-prog-l prog args))

(defun run-prog (prog &key args (wait t) (opts nil))
  "Common interface to shell. Does not return anything useful."
  #+sbcl (apply #'sb-ext:run-program prog args :wait wait :output *standard-output* opts)
  #-(or sbcl)
  (error 'not-implemented :proc (list 'run-prog prog opts)))

(defun pipe-output (prog &rest args)
  "Return an output stream which will go to the command."
  #+allegro (excl:run-shell-command (format nil "~a~{ ~a~}" prog args)
                                    :input :stream :wait nil)
  #+clisp (#+lisp=cl ext:make-pipe-output-stream
           #-lisp=cl lisp:make-pipe-output-stream
                     (format nil "~a~{ ~a~}" prog args))
  #+cmu (ext:process-input (ext:run-program prog args :input :stream
                                            :output t :wait nil))
  #+gcl (si::fp-input-stream (apply #'si:run-process prog args))
  #+lispworks (sys::open-pipe (format nil "~a~{ ~a~}" prog args)
                              :direction :output)
  #+lucid (lcl:run-program prog :arguments args :wait nil :output :stream)
  #+sbcl (sb-ext:process-input (sb-ext:run-program prog args :input :stream
                                                   :output t :wait nil))
  #-(or allegro clisp cmu gcl lispworks lucid sbcl)
  (error 'not-implemented :proc (list 'pipe-output prog args)))

(defun pipe-input (prog &rest args)
  "Return an input stream from which the command output will be read."
  #+allegro (excl:run-shell-command (format nil "~a~{ ~a~}" prog args)
                                    :output :stream :wait nil)
  #+clisp (#+lisp=cl ext:make-pipe-input-stream
           #-lisp=cl lisp:make-pipe-input-stream
                     (format nil "~a~{ ~a~}" prog args))
  #+cmu (ext:process-output (ext:run-program prog args :output :stream
                                             :error t :input t :wait nil))
  #+gcl (si::fp-output-stream (apply #'si:run-process prog args))
  #+lispworks (sys::open-pipe (format nil "~a~{ ~a~}" prog args)
                              :direction :input)
  #+lucid (lcl:run-program prog :arguments args :wait nil :input :stream)
  #+sbcl (sb-ext:process-output (sb-ext:run-program prog args :output :stream
                                                    :error t :input t :wait nil))
  #-(or allegro clisp cmu gcl lispworks lucid sbcl)
  (error 'not-implemented :proc (list 'pipe-input prog args)))

;;; Allegro CL: a simple `close' does NOT get rid of the process.
;;; The right way, of course, is to define a Gray stream `pipe-stream',
;;; define the `close' method and use `with-open-stream'.
;;; Unfortunately, not every implementation supports Gray streams, so we
;;; have to stick with this to further the portability.

(defun close-pipe (stream)
  "Close the pipe stream."
  (declare (stream stream))
  (close stream)
  #+allegro (sys:reap-os-subprocess))

(defmacro with-open-pipe ((pipe open) &body body)
  "Open the pipe, do something, then close it."
  `(let ((,pipe ,open))
    (declare (stream ,pipe))
    (unwind-protect (progn ,@body)
      (close-pipe ,pipe))))


;; Shell reader macro

(eval-when (:compile-toplevel :load-toplevel :execute)
  (set-macro-character #\] (get-macro-character #\)))
  (set-dispatch-macro-character #\# #\[
				(lambda (stream char1 char2)
				  (declare (ignore char1 char2))
				  (setf (readtable-case *readtable*) :preserve)
				  (unwind-protect
				       (let ((command-line (read-delimited-list #\] stream t)))
					 `(exec-prog-l ,(princ-to-string (car command-line))
						       ',(mapcar #'princ-to-string (rest command-line))))
				    (setf (readtable-case *readtable*) :upcase))))
  ) ; eval-when

