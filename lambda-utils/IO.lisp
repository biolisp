(in-package :lambda-utils)

(defparameter *default-external-format* :utf-8)

;;;
;;; Line oriented Processing
;;;

(defmacro each-line ((var stream) &body body)
  "Execute body for each line of stream"
  `(loop for ,var = (read-line ,stream nil nil)
      while ,var
      do ,@body))

(defun collect-lines (stream f)
  "Collect (f line) for the lines  of stream"
  (loop for line = (read-line stream nil nil)
     while line
     collect (funcall f line)))


;;;
;;; Read all file
;;;

(def-input-fun slurp (stream)
  (let ((seq (make-string (file-length stream))))
    (read-sequence seq stream)
    seq))


;;;
;;; *standard-output* and *standard-input*
;;;

(defmacro redirect (file &body body)
  `(with-open-file (*standard-output* ,file :direction :output :if-exists :supersede)
     ,@body))
