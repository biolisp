(in-package :lambda-utils)

(defconstant +default-print-head-lines+ 5 "Default number of lines printed by print-head")

(defgeneric print-head (obj &optional n)
  (:documentation "Print the first n items of the obj"))

(defmethod print-head ((filename string) &optional (n +default-print-head-lines+))
  (print-file-head filename n))

(defmethod print-head ((filename pathname) &optional (n +default-print-head-lines+))
  (print-file-head filename n))

(defmethod print-head ((filename string) &optional (n +default-print-head-lines+))
  (print-file-head filename n))


(defmethod print-head ((hash hash-table) &optional (n +default-print-head-lines+))
  (loop for k being the hash-key using (hash-value v) of hash 
        for i from 1 to n
        do (format t "~s =>~T~s~%" k v))
  (when (>= n (hash-table-count hash)) 
    (format t "...~%")))



(defmethod print-head ((vec simple-vector) &optional (n +default-print-head-lines+))
  (let ((dim (array-dimension vec 0)))
    (loop for i from 0 to (1- (min n dim))
       do (format t "~a~%" (aref vec i)))
    (when (> dim n) (format t "...~%"))))
  
  
(defun print-file-head (filename &optional (n +default-print-head-lines+))
  (with-open-file (file filename :direction :input)
    (loop for line = (read-line file nil :eof)
	  for i from 1 to n
	  while line
	  do (format t "~a~%" line))))
    
  