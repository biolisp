(in-package :bioinfo)

(defparameter *ucsc-dir* 
  (merge-pathnames  #p"lbhc/ucsc/db/" (user-homedir-pathname)))

(defmacro def-ucsc-file-mapper ((name file) &body body)  
  `(defun ,name (fun &key (file (merge-pathnames *ucsc-dir* ,file)) limit) 
     ,(concatenate 'string "Read UCSC " file " file")
     (let ((count 1))
       (with-open-typed-file (stream file)
	 (each-line (line stream)
	   (let ((data (split-sequence-array #\Tab line)))
	     (when (and limit (= count limit))
	       (return-from ,name))
	     (incf count)
	     (funcall fun (progn
			    ,@body))))))))


(def-ucsc-file-mapper (map-chrom-info "chromInfo.txt.gz")
  (list
   (make-instance 'feature
		  :seqid (aref data 0)
		  :name (aref data 0)
		  :start 1
		  :strand "+"
		  :end (read-from-string (aref data 1))
		  :source "UCSC"
		  :type "contig")
   (make-instance 'feature
		  :seqid (aref data 0)
		  :id (aref data 0)
		  :name (aref data 0)
		  :start 1
		  :strand "+"
		  :end (read-from-string (aref data 1))
		  :source "UCSC"
		  :type "chromosome")))

;; TODO: Guess db from id

(defparameter *dbxref-db-patterns*
  (mapcar #'(lambda (pair)
	      (list
	       (create-scanner (first pair))
	       (second pair)))
	  '(("^NP_" :NCBI_NP)
	    ("^[A-Z][A-Z0-9]{5}(-[0-9]+)?$" :protein_id)
	    ("^[0-9]+$" :NCBI_gi)
	    ("^NM_" :NCBI_NM))))
   
(defun dbxref-db (id)
  (dolist (pair *dbxref-db-patterns*)
    (when (scan (first pair) id)
      (return-from dbxref-db (second pair))))
  (return-from dbxref-db :unknow))
    
(defun format-dbxref (id &optional db)
  (format nil "~a:~a"  (if db db (dbxref-db id)) id))

(defun ucsc-know-gene-make-exons (data)
  (let ((starts (remove "" (split-sequence #\, (aref data 8)) :test #'equal))
	(ends  (split-sequence #\, (aref data 9)))
	(i 0))
    
    (mapcar #'(lambda (start end)
		(make-instance 'feature
			       :seqid (aref data 1)
			       :id (format nil "exon_~a_~d" (aref data 0) (incf i))
			       :start (read-from-string start)
			       :end (read-from-string end)
			       :strand (aref data 2)
			       :source "UCSC"
			       :type "exon"))    
	     starts ends)))


(defun ucsc-know-gene-make-mRNA (data)
  (list
   (make-instance 'feature
		  :seqid (aref data 1)	; chrom
		  :id (format nil "mrna_~a" (aref data 0))
		  :name (aref data 0)
		  :strand (aref data 2)			    ; strand
		  :start (read-from-string (aref data 5))    ; cdsStart
		  :end (read-from-string (aref data 6))	    ; cdsEnd
		  :source "UCSC"
		  :children (ucsc-know-gene-make-exons data)
		  :type "mRNA")))

		    
(def-ucsc-file-mapper (map-know-gene "knownGene.txt.gz")
  (let ((dbxrefs nil))    
    (when (> (length (aref data 10)) 0)
      (push (format-dbxref (aref data 10)) dbxrefs))
    (push (format-dbxref (aref data 11) :ucsc_align) dbxrefs)
    
    (make-instance 'feature
		   :seqid (aref data 1) ; chrom
		   :id (aref data 0)
		   :name (aref data 0)
		   :strand (aref data 2)  ; strand
		   :start (read-from-string (aref data 3)) ; txStart
		   :end (read-from-string (aref data 4))    ; txEnd
		   :source "UCSC"
		   :children (ucsc-know-gene-make-mRNA data)
		   :type "gene"
		   :dbxrefs dbxrefs)))


(def-output-fun convert-gff (stream)
  (flet ((save-gff (feat)
	   (gff feat stream)))
    (puts "##gff-version 3" stream)
    (map-chrom-info #'save-gff)
    (map-know-gene #'save-gff :limit 100)))
