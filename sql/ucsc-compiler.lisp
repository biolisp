(in-package :bioinfo)

(defparameter *ucsc-dir* 
  (merge-pathnames  #p"lbhc/ucsc/db/" (user-homedir-pathname)))

(defparameter *gen-target*   (merge-pathnames *system-path* #p"ucsc-gen.lisp"))

(defun ucsc-map-sql (fn)
  (cl-fad:walk-directory *ucsc-dir* fn
			 :test #'(lambda (x)
				   (equal (pathname-type x) "sql"))))

;; Generate CLSQL classes for sql files of UCSC
(defun compile-ucsc-sql ()
  (redirect *gen-target*
    (write '(in-package :bioinfo))
    (terpri)
    (ucsc-map-sql #'(lambda (x)
		      (format t ";; Generated from ~a~%" (namestring x))
		      (write (compile-sql x))
		      (terpri))))
  (compile-file *gen-target*))

(defun compile-and-load-ucsc ()
  (compile-ucsc-sql)
  (load (make-pathname :type "fasl" :defaults *gen-target*)))

