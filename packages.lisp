(in-package :cl-user)

(defpackage :dataset
  (:use :cl :lambda-utils)) 

(defpackage :bioinfo
  (:use :cl :lambda-utils :gzip-stream :dataset :cl-utilities :cl-ppcre :lispbuilder-lexer :yacc :clsql)
   (:nicknames :bio)
   (:export *system-path*
	    *ucsc-dir*
	    load-sagemap-file
	    load-sagemap
	    annotate-tags-file))
   

(defpackage :bio-user
  (:use :bioinfo :cl :lambda-utils :cl-ppcre :cl-utilities))

(defpackage :ucsc
  (:use :bioinfo :cl :clsql))
