(defpackage #:bioinfo-system
  (:use :cl :asdf))

(in-package #:bioinfo-system)

(defsystem :bioinfo  
  :description "A (hope so) complete system for bioinformatics analysis."
  :version "0.1"
  :author "Leonardo Varuzza <varuzza@gmail.com>"
  :licence "GPL"
  :depends-on (:cl-ppcre :cl-fad :cl-utilities :lambda-utils :lispbuilder-regex :lispbuilder-lexer :yacc :clsql)
  :serial t
  :components ((:file "packages")
	       (:file "os")
	       (:file "dataset")
	       (:file "bioinfo")
	       (:module "sage"
			:components ((:file "sage")))
	       (:file "feature")

;; 	       (:module "sql"
;; 			:components 
;; 			((:file "sql")
;; 			 (:file "ucsc-compiler")
;; 			 (:file "ucsc-gen")
;; 			 (:file "ucsc-sql")))

;; 	       (:module "graphics"
;; 			:components ((:file "packages")
;; 				     (:file "util")
;; 				     (:file "panel")))
;; 	       (:module "cluster"
;; 			:components ((:file "packages")
;; 				     (:file "distance")))))
