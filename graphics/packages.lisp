(in-package :bioinfo)

(defpackage :bio-graphics
  (:use :cl :vecto :bioinfo)
  (:export panel))
