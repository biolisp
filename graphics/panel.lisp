(in-package :bio-graphics)

(defclass panel ()
  ((title   :initarg :title  :accessor title-of)
   (width   :initarg :width  :accessor width-of)
   (height  :initarg :height :accessor height-of)
   (tracks  :initarg :tracks :accessor tracks-of))
  (:documentation "A panel to draw sequence alignment and informations, in the style of Bio::Graphics of bioperl"))


