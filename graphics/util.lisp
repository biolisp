(in-package :bio-graphics)

(defun default-font ()
  (namestring (merge-pathnames *system-path* #p"times.ttf")))
